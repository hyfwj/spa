# Examples for XML HttpRequests
This project contains one server, responding to requests and one or more clients sending requests to this server.
The server is written with NodeJS.

### Setup and run
You will need to have `node` and `npm` installed for the following commands to work.

### Server
First fire up the server, else nothing will handle/serve your requests.
* `cd server` In terminal go to root directory (containing app.js and package.json)
* `npm install` Reads the package.json file and installs dependencies into folder `node_modules`
* `node app.js` Starts up the server that is now listening on some port on localhost

### Clients
Now you can start your favorite browser and go to http://localhost:3005. The server will give you the Main Menu 
-- a list of clients/applications, choose one.